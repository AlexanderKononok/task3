//
//  ViewController.swift
//  myContactsApp
//
//  Created by Alexander Kononok on 9/27/21.
//

import UIKit
import Contacts
import ContactsUI
import CoreData

class ViewController: UIViewController, CNContactPickerDelegate {

    private let mainView = UIView()
    private let buttonsView = UIView()
    private let chooseContactButton = UIButton()
    private let showContactsButton = UIButton()
    private let showContactNumberButton = UIButton()
    
    var phoneNumber = String()
    var firstName = String()
    var secondName = String()
    var emailContact: NSString? = nil
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        addMainView()
        addButtons()
    }
    
    func addMainView() {
        view.addSubview(mainView)
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        mainView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        mainView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    func addButtons() {
        mainView.addSubview(buttonsView)
        buttonsView.translatesAutoresizingMaskIntoConstraints = false
        buttonsView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor).isActive = true
        buttonsView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor).isActive = true
        buttonsView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
        buttonsView.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.5).isActive = true
        
        let cornerRadius: CGFloat = 16
        let paddingButton: CGFloat = 10
        let fontSize: CGFloat = 30
        let colorButton: UIColor = .purple
        
        buttonsView.addSubview(chooseContactButton)
        chooseContactButton.backgroundColor = colorButton
        chooseContactButton.setTitle("Выбрать контакт", for: .normal)
        chooseContactButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
        chooseContactButton.layer.cornerRadius = cornerRadius
        chooseContactButton.translatesAutoresizingMaskIntoConstraints = false
        chooseContactButton.topAnchor.constraint(equalTo: buttonsView.topAnchor).isActive = true
        chooseContactButton.leadingAnchor.constraint(equalTo: buttonsView.leadingAnchor, constant: paddingButton).isActive = true
        chooseContactButton.trailingAnchor.constraint(equalTo: buttonsView.trailingAnchor, constant: -paddingButton).isActive = true
        chooseContactButton.addTarget(self, action: #selector(chooseContactButtonPressed), for: .touchUpInside)
        
        buttonsView.addSubview(showContactsButton)
        showContactsButton.backgroundColor = colorButton
        showContactsButton.setTitle("Показать контакты", for: .normal)
        showContactsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
        showContactsButton.layer.cornerRadius = cornerRadius
        showContactsButton.translatesAutoresizingMaskIntoConstraints = false
        showContactsButton.topAnchor.constraint(equalTo: chooseContactButton.bottomAnchor, constant: paddingButton).isActive = true
        showContactsButton.leadingAnchor.constraint(equalTo: buttonsView.leadingAnchor, constant: paddingButton).isActive = true
        showContactsButton.trailingAnchor.constraint(equalTo: buttonsView.trailingAnchor, constant: -paddingButton).isActive = true
        showContactsButton.heightAnchor.constraint(equalTo: chooseContactButton.heightAnchor).isActive = true
        showContactsButton.addTarget(self, action: #selector(showContactsButtonPressed), for: .touchUpInside)
        
        buttonsView.addSubview(showContactNumberButton)
        showContactNumberButton.backgroundColor = colorButton
        showContactNumberButton.setTitle("Показать номер", for: .normal)
        showContactNumberButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
        showContactNumberButton.layer.cornerRadius = cornerRadius
        showContactNumberButton.translatesAutoresizingMaskIntoConstraints = false
        showContactNumberButton.topAnchor.constraint(equalTo: showContactsButton.bottomAnchor, constant: paddingButton).isActive = true
        showContactNumberButton.leadingAnchor.constraint(equalTo: buttonsView.leadingAnchor, constant: paddingButton).isActive = true
        showContactNumberButton.trailingAnchor.constraint(equalTo: buttonsView.trailingAnchor, constant: -paddingButton).isActive = true
        showContactNumberButton.bottomAnchor.constraint(equalTo: buttonsView.bottomAnchor).isActive = true
        showContactNumberButton.heightAnchor.constraint(equalTo: chooseContactButton.heightAnchor).isActive = true
        showContactNumberButton.addTarget(self, action: #selector(showContactNumberButtonPressed), for: .touchUpInside)
        
    }
    
    @objc func chooseContactButtonPressed() {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        present(contactPicker, animated: true, completion: nil)
    }
    
    @objc func showContactsButtonPressed() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ContactEntity")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            
            for data in result as! [NSManagedObject] {
                if let phone = data.value(forKey: "phone") {
                    print("\(phone)")
                }
                if let name = data.value(forKey: "name") {
                    print("\(name)")
                }
                if let secondName = data.value(forKey: "secondName") {
                    print("\(secondName)")
                }
                if let email = data.value(forKey: "email") {
                    print("\(email)")
                }
            }
        } catch {
            print("Failed retrieved")
        }
        
    }
    
    @objc func showContactNumberButtonPressed() {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        self.phoneNumber = String()
        for phone in contact.phoneNumbers {
            phoneNumber = phone.value.stringValue
        }
        self.firstName = contact.givenName
        self.secondName = contact.familyName
        self.emailContact = nil
        
        for email in contact.emailAddresses {
            emailContact = email.value
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "ContactEntity", in: context)
        let newContact = NSManagedObject(entity: entity!, insertInto: context)
        
        newContact.setValue("\(phoneNumber)", forKey: "phone")
        newContact.setValue("\(firstName)", forKey: "name")
        newContact.setValue("\(secondName)", forKey: "secondName")
        if let email = emailContact {
            newContact.setValue("\(email)", forKey: "email")
        }
        
        do {
            try self.context.save()
            print("Save data")
        } catch let error {
            print("Saving Failed \(error)")
        }
        
    }
    
    
}

